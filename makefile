CC = g++
CFLAGS = -c -Wall
LDFLAGS = 
AL_VERSION = 5.0.7
LIB_DIR = /usr/local/lib
LIBRARIES = $(LIB_DIR)/liballegro.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_image.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_acodec.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_audio.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_ttf.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_font.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_primitives.so.$(AL_VERSION) \
            $(LIB_DIR)/liballegro_dialog.so.$(AL_VERSION)
SOURCE_EXT = cpp
SOURCES = $(wildcard *.$(SOURCE_EXT))
OBJECTS =$(SOURCES:.$(SOURCE_EXT)=.o)
OBJECTS_DIRECTORY = './objects/'
EXTERN_OBJECTS_DIRECTORY = './extern-obj'
EXECUTABLE = main

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS)  -o $@ $(LIBRARIES)
	mv *.o $(OBJECTS_DIRECTORY)

.(SOURCE_EXT).o:
	$(CC) $(CFLAGS) $<

clean:
	rm -rf $(OBJECTS_DIRECTORY)*.o $(EXECUTABLE)

install:
	mv $(EXECUTABLE) /usr/local/bin

uninstall:
	rm -rf /usr/local/bin/$(EXECUTABLE)
