

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>

#include <iostream>
#include <vector>
#include <time.h>
#include <stdio.h>

using namespace :: std;

ALLEGRO_DISPLAY      *display          = NULL;
ALLEGRO_TIMER        *timer            = NULL;
ALLEGRO_EVENT_QUEUE  *event_queue      = NULL;
ALLEGRO_FONT         *font             = NULL;

int SCREEN_W      = 840;
int SCREEN_H      = 640;

const float FPS = 60.0;

const int 
          TAM_MAX      = 15,
          TAM_MIN      = 3,
          SPHERE_MOV   = 3,
          SPHERE_MAX_X = SCREEN_W - (SCREEN_W/20),
          SPHERE_MAX_Y = SCREEN_H - (SCREEN_H/20),
          SPHERE_MIN_X = 20,
          SPHERE_MIN_Y = 20,
          COLOR_MOV    = 5,
          MAX_COLISION_BOOST   = 8,
          MINUS_COLISION_BOOST = 1;

volatile int  
              SPHERE_BOOST    = 2;

bool          PRINT_CONNECTIONS = true,
              SHOW_SPHERES      = false,
              FILLED_SPHERES    = true;
long int      LIMIT             = 30;


#include "src/String.cpp"
#include "src/Keys.h"
#include "src/Color.h"

struct SPHERES
{
       long int     x, y;
       
       short int    tam,
                    directionX, directionY,
                    color_r, color_g, color_b,
                    spd,
                    colision_boost;
                    
       bool         min_x,
                    max_x,
                    min_y,
                    max_y;
};
vector <SPHERES> list_spheres;
vector <SPHERES> :: iterator it_spheres;


void createSpheres()
{
     long int    i,
                 tam2 = TAM_MIN,
                 x = SCREEN_W/2, y = SCREEN_H/2,
                 dif_x, dif_y;
     short int   dif;
     SPHERES    *ptr_sphere,
                 aux_sphere;
     
     for(i = 0;i < LIMIT;i++)
     {
       ptr_sphere = new (SPHERES);
       
       dif = rand() % 2;
       dif_x = rand() % 100;
       dif_y = rand() % 100;
       
       if(dif == 1)
       {
         ptr_sphere->x = x + dif_x;
         ptr_sphere->y = y + dif_y;
       }
       
       else
       {
         ptr_sphere->x = x - dif_x;
         ptr_sphere->y = y - dif_y;
       }
       
       ptr_sphere->tam = tam2;
       
       ptr_sphere->color_r = rand() % 255 + 1;
       ptr_sphere->color_g = rand() % 255 + 1;
       ptr_sphere->color_b = rand() % 255 + 1;
       
       ptr_sphere->spd = rand() % 10 + 1;
       
       ptr_sphere->min_x = 
       ptr_sphere->max_x =
       ptr_sphere->min_y =
       ptr_sphere->max_y = false;
       ptr_sphere->directionX = ptr_sphere->directionY = 1;
       
       if(tam2 >= TAM_MAX)
         tam2 = TAM_MIN;
         
       else
         tam2 += 2;
       
       aux_sphere = (*ptr_sphere);
       
       list_spheres.push_back(aux_sphere);
       
       free (ptr_sphere);
     }
}


void ChangeColor()
{
      short int   color = rand() % 3;
      vector <SPHERES> :: iterator i;
      
   for(i = list_spheres.begin();i != list_spheres.end();i++)
   {
      if(color == 0)
      {
		  if(i->color_r + COLOR_MOV < 255)
		    i->color_r = rand() % 255 + 1;
		  
		  else
		    i->color_r += COLOR_MOV;
      }
      
      else if(color == 1)
      {
		  if(i->color_g + COLOR_MOV < 255)
		    i->color_g = rand() % 255 + 1;
		  
		  else
		    i->color_g += COLOR_MOV;
      }
      
      else if(color == 2)
      {
		  if(i->color_b + COLOR_MOV < 255)
		    i->color_b = rand() % 255 + 1;
		  
		  else
		    i->color_b += COLOR_MOV;
      }
  }
}


int printSpheres()
{
   short int                boost;
   vector <SPHERES> :: iterator it;
   
   for(it_spheres = list_spheres.begin();it_spheres != list_spheres.end();it_spheres++)
   {
     if(it_spheres->x + it_spheres->tam >= SCREEN_W)
       it_spheres->directionX = 0;
     
     else if(it_spheres->x <= 0)
       it_spheres->directionX = 1;
       
     if(it_spheres->y + it_spheres->tam >= SCREEN_H)
       it_spheres->directionY = 0;
     
     else if(it_spheres->y <= 0)
       it_spheres->directionY = 1;
   }
   
   for(it_spheres = list_spheres.begin();it_spheres != list_spheres.end();it_spheres++)
   {
	 if(SHOW_SPHERES)
	 {
		 if(FILLED_SPHERES)
		   al_draw_filled_circle(
		     it_spheres->x, it_spheres->y, it_spheres->tam,
		     al_map_rgb(it_spheres->color_r, it_spheres->color_g, it_spheres->color_b));
		 else
		   al_draw_circle(
		     it_spheres->x, it_spheres->y, it_spheres->tam,
		     al_map_rgb(it_spheres->color_r, it_spheres->color_g, it_spheres->color_b), 2);
	 }
      
     if(PRINT_CONNECTIONS)
     {
       if(it_spheres+1 != list_spheres.end())
       {
         it = it_spheres+1;
         al_draw_line(
             it_spheres->x, 
             it_spheres->y, 
             it->x, 
             it->y, 
             al_map_rgb(it_spheres->color_r, it_spheres->color_g, it_spheres->color_b), 2);
       }
     }
	   boost = rand() % SPHERE_BOOST + 1;
	   
	   if(it_spheres->directionX == 0)
	   {
		 if(it_spheres->colision_boost == 0)
	         it_spheres->x -= it_spheres->spd + boost;
	     else
	     {
	         it_spheres->x -= it_spheres->spd + it_spheres->colision_boost + boost;
	         it_spheres->colision_boost -= MINUS_COLISION_BOOST;
	     }
	   }
	   
	   else
	   {
		 if(it_spheres->colision_boost == 0)
	         it_spheres->x += it_spheres->spd + boost;
	     else
	     {
	         it_spheres->x -= it_spheres->spd + it_spheres->colision_boost + boost;
	         it_spheres->colision_boost -= MINUS_COLISION_BOOST;
	     }
	   }
	     
	   if(it_spheres->directionY == 0)
	   {
		 if(it_spheres->colision_boost > 0)
	         it_spheres->y -= it_spheres->spd + boost;
	     else
	     {
	         it_spheres->y -= it_spheres->spd + it_spheres->colision_boost + boost;
	         it_spheres->colision_boost -= MINUS_COLISION_BOOST;
	     }
	   }
	   
	   else
	   {
		 if(it_spheres->colision_boost > 0)
	         it_spheres->y += it_spheres->spd + boost;
	     else
	     {
	         it_spheres->y += it_spheres->spd + it_spheres->colision_boost + boost;
	         it_spheres->colision_boost -= MINUS_COLISION_BOOST;
	     }
	   }
	   
	   if (it_spheres->colision_boost < 0)
	       it_spheres->colision_boost = 0;
	   
	   /*
	    * Verifica colisão com outras esferas
	    */
	   for(it = list_spheres.begin();it != list_spheres.end();it++)
	   {   /* L >> R */
		   if(it_spheres->x < it->x)
		   {
			   if(it_spheres->x + it_spheres->tam >= it->x)
			   {   /* U >> D */
				   if(it_spheres->y < it->y)
				   {
					   if(it_spheres->y + it_spheres->tam >= it->y)
					   {
						   if(it_spheres->directionX)
						     it_spheres->directionX = 0;
						   else
						     it_spheres->directionX = 1;
						     
						   if(it_spheres->directionY)
						     it_spheres->directionY = 0;
						   else
						     it_spheres->directionY = 1;
						     
						   if(it->directionX)
						     it->directionX = 0;
						   else
						     it->directionX = 1;
						     
						   if(it->directionY)
						     it->directionY = 0;
						   else
						     it->directionY = 1;
						   
						   it->colision_boost = MAX_COLISION_BOOST;
						     
						   break;
					   }
				   }
				      /* D >> U */
				   if(it_spheres->y > it->y)
				   {
					   if(it_spheres->y <= it->y + it->tam)
					   {
						   if(it_spheres->directionX)
						     it_spheres->directionX = 0;
						   else
						     it_spheres->directionX = 1;
						     
						   if(it_spheres->directionY)
						     it_spheres->directionY = 0;
						   else
						     it_spheres->directionY = 1;
						     
						   if(it->directionX)
						     it->directionX = 0;
						   else
						     it->directionX = 1;
						     
						   if(it->directionY)
						     it->directionY = 0;
						   else
						     it->directionY = 1;
						     
						   break;
					   }
				   }
			   }
		   }
		      /* R >> E */
		   if(it_spheres->x > it->x)
		   {
			   if(it_spheres->x <= it->x + it->tam)
			   {   /* U >> D */
				   if(it_spheres->y < it->y)
				   {
					   if(it_spheres->y + it_spheres->tam >= it->y)
					   {
						   if(it_spheres->directionX)
						     it_spheres->directionX = 0;
						   else
						     it_spheres->directionX = 1;
						     
						   if(it_spheres->directionY)
						     it_spheres->directionY = 0;
						   else
						     it_spheres->directionY = 1;
						     
						   break;
					   }
				   }
				      /* D >> U */
				   if(it_spheres->y > it->y)
				   {
					   if(it_spheres->y <= it->y + it->tam)
					   {
						   if(it_spheres->directionX)
						     it_spheres->directionX = 0;
						   else
						     it_spheres->directionX = 1;
						     
						   if(it_spheres->directionY)
						     it_spheres->directionY = 0;
						   else
						     it_spheres->directionY = 1;
						     
						   break;
					   }
				   }
			   }
		   }
	   }
   }
   
   if(PRINT_CONNECTIONS)
   {
       it_spheres = list_spheres.begin();
       it = list_spheres.end()-1;
   
       al_draw_line(
             it_spheres->x, 
             it_spheres->y, 
             it->x, 
             it->y, 
             al_map_rgb(it_spheres->color_r, it_spheres->color_g, it_spheres->color_b), 2);
	}
   
   return (1);
}


int main(int argc, char** argv)
{
    char                        key_char[100];
    bool                        exit           = false;
    bool                        redraw         = true;
    volatile int                key_sphere = 0;
    ALLEGRO_TIMER              *timer_change_color = NULL,
                               *timer_change_filled = NULL;
    ALLEGRO_MONITOR_INFO        info;
    
    //cout << "Enter SPHEREs limit: ";
    //cin >> LIMIT;
    
    if(!al_init())
    {
        fprintf(stderr, "Failed to initialize Allegro!\n");
        return -1;
    }
    
    srand( time(NULL) );
    
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();
    
    createSpheres();

    /*font   = al_load_ttf_font("resource/font01.ttf", 32, 0);
    if(font == NULL)
    {
        fprintf(stderr, "Failed to load font!\n");
        return -1;
    }*/

    al_get_monitor_info(0, &info); 
    //SCREEN_W = info.x2 - info.x1; /* Assume this is 1366 */ 
    //SCREEN_H = info.y2 - info.y1; /* Assume this is 768 */ 
    SCREEN_H = SCREEN_W = 800;
    
    display = al_create_display(SCREEN_W, SCREEN_H);
    if(!display)
    {
        fprintf(stderr, "Failed to create display!\n");
        return -1;
    }
    al_set_target_bitmap(al_get_backbuffer(display));

    timer = al_create_timer(1.0 / FPS);
    if(!timer)
    {
        fprintf(stderr, "Failed to create timer!\n");
        al_destroy_display(display);
        return -1;
    }
    
    timer_change_color = al_create_timer(1.0/FPS);
    if(!timer_change_color)
    {
        fprintf(stderr, "Failed to create timer!\n");
        al_destroy_display(display);
        return -1;
    }
    
    timer_change_filled = al_create_timer(5.0);
    if(!timer_change_filled)
    {
        fprintf(stderr, "Failed to create timer!\n");
        al_destroy_display(display);
        return -1;
    }

    event_queue = al_create_event_queue();
    if(!event_queue)
    {
        fprintf(stderr, "Failed to create event queue!\n");
        al_destroy_timer(timer);
        al_destroy_display(display);
        return -1;
    }

    al_install_mouse();
    al_install_keyboard();

    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_timer_event_source(timer_change_color));
    al_register_event_source(event_queue, al_get_timer_event_source(timer_change_filled));

    if( al_is_keyboard_installed() )
      al_register_event_source(event_queue, al_get_keyboard_event_source());

    if( al_is_mouse_installed() )
      al_register_event_source(event_queue, al_get_mouse_event_source());
    
    al_hide_mouse_cursor(display);

    al_clear_to_color(BLACK);

    al_flip_display();
    
    al_start_timer(timer);
    al_start_timer(timer_change_color);
    al_start_timer(timer_change_filled);

    while(!exit)
    {
        ALLEGRO_EVENT ev;

        al_wait_for_event(event_queue, &ev);

        switch(ev.type)
        {
          case ALLEGRO_EVENT_DISPLAY_CLOSE:
            exit = true;
            break;
        
          case ALLEGRO_EVENT_TIMER:
            if(ev.timer.source == timer)
              redraw = true;
            
            else if(ev.timer.source == timer_change_color)
              ChangeColor();
             
            else if(ev.timer.source == timer_change_filled)
              FILLED_SPHERES = !FILLED_SPHERES;
            
            break;
        
          case KEY_DOWN:
            strcpy(key_char, al_keycode_to_name(ev.keyboard.keycode));
        
            if( StringCompareNoCase(key_char, "escape") )
              exit = true;
              
            else if( StringCompareNoCase(key_char, "b") )
            {
              SHOW_SPHERES = !SHOW_SPHERES;
              
              if(SHOW_SPHERES)
                PRINT_CONNECTIONS = false;
              else
                PRINT_CONNECTIONS = true;
		    }
		    
            else if( StringCompareNoCase(key_char, "f") )
              FILLED_SPHERES = !FILLED_SPHERES;
		    
            else if( StringCompareNoCase(key_char, "down") )
            {
				if(SPHERE_BOOST > 1)
				    SPHERE_BOOST--;
			}
          
            else if( StringCompareNoCase(key_char, "up") )
            {
				SPHERE_BOOST++;
			}
        
            else if( StringCompareNoCase(key_char, "right") ){ }
           
            else if( StringCompareNoCase(key_char, "left") ){ }
            
            else if( StringCompareNoCase(key_char, "space") )
              PRINT_CONNECTIONS = !PRINT_CONNECTIONS;
        
          break;
      
      /**************************
          Soltou uma tecla
      ***************************/
          case KEY_UP:
            strcpy(key_char, al_keycode_to_name(ev.keyboard.keycode));
            break;
          
          case ALLEGRO_EVENT_MOUSE_AXES:/* ev.mouse.x */
            break;
        }
        
        if(redraw && al_is_event_queue_empty(event_queue))
        {
            redraw = false;

            al_clear_to_color(BLACK);
            
            printSpheres();
            
            key_sphere = 0;
            
            al_flip_display();
        }
    }

    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);

    return 0;
}

