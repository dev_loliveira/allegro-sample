

#ifndef COLOR_H
#define COLOR_H

#include <allegro5/allegro_color.h>

#define BLACK               al_map_rgb(0, 0, 0)
#define RED                 al_map_rgb(255, 0, 0)
#define WHITE               al_map_rgb(255, 255, 255)
#define NAVYBLUE            al_map_rgb(0, 0, 128)
#define YELLOW              al_map_rgb(255, 255, 0)
#define GREY                al_map_rgb(190, 190, 190)
#define GREEN               al_map_rgb(0, 255, 0)
#define DARKGREEN           al_map_rgb(0, 100, 0)
#define CYAN                al_map_rgb(0, 255, 255)
#define MAROON              al_map_rgb()176, 48, 96

#define INVISIBLE           al_map_rgb(255, 0, 255)


#endif
