
#ifndef STRING_CPP
#define STRING_CPP
/*******************************************
  Header que possui alguns metodo que
  auxiliam a manipular string
*******************************************/

#include <math.h>


char* reverse(char *str)
{
  volatile int   i, i2;
  int            lengh;
  char          *result,
                 aux[100];

  i2     = 0;
  lengh  = strlen(str)-1;

  for(i = lengh;i >= 0;i--)
    aux[i2++] = str[i];
  aux[i2] = '\0';

  result = (char *)malloc(lengh + 2);
  strcpy(result, aux);

  return (result);
}


char* itoa(int n, int actualBase, int base)
{
  char         *result,
                rests[500],
                strN[100];
  volatile int  i, i2,
                expo;
  short int     digit;
  int           sum;


  sprintf(strN, "%d", n);

  if(actualBase != base)
  {
    if(actualBase != 10)
    {
      /* Convert to base 10 */
      expo = sum = 0;
      for(i = strlen(strN)-1;i >= 0;i--)
      {
        digit = (int)strN[i] - 48;
        sum  += digit * (pow(actualBase, expo++));
      }

      n = sum;
    }

    if(base == 10)
    {
      sprintf(strN, "%d", n);
      result = (char *)malloc(strlen(strN) + 1);
      strcpy(result, strN);
    }
    /* Convert the number on base 10, to the paramter destiny base */
    else
    {
      i2 = 0;
      while(n != 0)
      {
        rests[i2++] = (char)( (n % base) + 48 );
        n /= base;
      }
      rests[i2] = '\0';

      result = reverse(rests);
    }
  }

  else
  {
    result = (char *)malloc(strlen(strN) + 1);
    strcpy(result, strN);
  }

  return (result);
}




char* toString(int n)
{
  char *aux,
        value[50];

  aux = itoa(n, 10, 10);
  
  aux = (char *)malloc(strlen(value) + 1);
  strcpy(aux, value);
  
  return (aux);
}



unsigned int StringLengh(char *str)
{
  unsigned int i;
  for(i = 0;str[i] != '\0';i++){}
  return (i);
}


bool StringCompare(char *str, char *str2)
{
  unsigned int i;
  
  if( (StringLengh(str) < StringLengh(str2)) || (StringLengh(str2) < StringLengh(str)) )
    return (false);
  
  for(i = 0;str[i] != '\0';i++)
      if(str2[i] != str[i])
        return (false);

  return (true);
}

/*
  Comparacao de strings nao sendo
  case-sensitive
*/
bool StringCompareNoCase(char *str, char *str2)
{
  unsigned int i;
  
  if( (StringLengh(str) < StringLengh(str2)) || (StringLengh(str2) < StringLengh(str)) )
    return (false);
  
  for(i = 0;str[i] != '\0';i++)
  {
      if(str2[i] != str[i])
      {
        /*Caractere da primeira string � UpperCase*/
        if((int)str[i] >= 65 && (int)str[i] <= 90)
        {
          if( (int)str[i] != (int)(str2[i] - 32) )
            return (false);
        }
        
        /*Caractere da segunda string � UpperCase*/  
        else if((int)str2[i] >= 65 && (int)str2[i] <= 90)
        {
          if( (int)(str[i] - 32) != (int)str2[i] )
            return (false);
        }
            
        /*Caracters simplesmente sao diferentes*/
        else
          return (false);
      }
  }
  
  return (true);
}

#endif
